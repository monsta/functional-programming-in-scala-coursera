### What is this repository for? ###

This is the "getting started" example project in Function Programming Principles class in Coursera.

I also use this project to try some simple code snippet during the class.

in terminal, use one of following commands to enter the REPL
 + scala
 + sbt console