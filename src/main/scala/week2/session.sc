object exercise {

    def factorialOrigin(n: Int): Int = {
        if (n == 0) 1
        else n * factorialOrigin(n-1)
    }
    factorialOrigin(5)
    factorialOrigin(7)
    def factorialTailRec(n: Int): Int = {
//        把function最後要的結果的公式長相，放在acc這個參數
//        把recursion count放在n
//        在loop的termination條件(第一個if)回傳acc
        def loop(acc: Int, n: Int): Int =
            if (n == 0) acc
            else loop(acc * n, n-1)
        loop(1, n)
    }

    factorialTailRec(4)
    factorialTailRec(5)
    def sumTailRecursion(f: Int => Int)(a: Int, b: Int): Int = {
        def loop(a: Int, acc: Int): Int = {
            if (a > b) acc
            else loop(a+1, acc + f(a))
        }
        loop(a, 0)
    }
    sumTailRecursion(x=>x*x*x)(2, 4)
    sumTailRecursion(factorialTailRec)(2, 4)
    sumTailRecursion(x=>x * x)(3, 5)

    def sum(f: Int => Int, a: Int, b: Int): Int =
        if (a > b) 0
        else f(a) + sum(f, a+1, b)
    def id(a: Int): Int = a
    def sumInts(a: Int, b: Int)     = sum(id, a, b)
    sumInts(4, 9)
    def cube(m: Int) = m * m * m
    def sumCubes(a: Int, b: Int)    = sum(cube, a, b)
    sumCubes(2, 4)
    def sumCubesWithAnonymousFunction(a: Int, b: Int) =
        sum(x => x * x * x, a, b)
    sumCubesWithAnonymousFunction(2, 4)
}