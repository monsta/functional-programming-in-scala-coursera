object session {
    def sum(f: Int => Int): (Int, Int) => Int = {
        def sumF(a: Int, b: Int): Int = {
            if (a > b) 0
            else f(a) + sumF(a+1, b)
        }
        sumF
    }

    sum(x=>x*x)(2, 5)

    def sumInts = sum(x => x)
    sumInts(1, 3)

    def sumCubes = sum(x => x * x * x)
    sumCubes(1, 3)
}