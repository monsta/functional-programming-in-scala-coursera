object intsets {
    val t1 = new NonEmpty(3, Empty, Empty)        //> t1  : intsets.NonEmpty = {.3.}
    val t2 = t1 incl 4                            //> t2  : intsets.IntSet = {.3{.4.}}
    Empty contains 1                              //> res0: Boolean = false
    new NonEmpty(7, Empty, Empty) contains 7      //> res1: Boolean = true
    
    abstract class IntSet {
        def incl(x: Int): IntSet
        def contains(x: Int): Boolean
        def union(other: IntSet): IntSet
    }
    object Empty extends IntSet {
        def incl(x: Int): IntSet =
            new NonEmpty(x, Empty, Empty)
        def contains(x: Int): Boolean = false

        override def toString: String = "."
        def union(other: IntSet): IntSet = other
    }
    class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
        override def incl(x: Int): IntSet = {
            if (x < elem) new NonEmpty(elem, left incl x, right)
            else if (x > elem) new NonEmpty(elem, left, right incl x)
            else this
        }
        override def contains(x: Int): Boolean = {
            if (x < elem) left contains x
            else if (x > elem) right contains x
            else true
        }

        override def toString: String = "{" + left + elem + right + "}"

        def union(other: IntSet): IntSet =
            ((left union right) union other) incl elem
    }

}